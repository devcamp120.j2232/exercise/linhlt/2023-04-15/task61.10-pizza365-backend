package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.Drink;
import com.devcamp.pizza365.repository.DrinkRepository;

@Service
public class DrinkService {
    @Autowired
    DrinkRepository drinkRepository;
    public List<Drink> getAllDrinks(){
        List<Drink> drinkList = new ArrayList<Drink>();
        drinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }
}
