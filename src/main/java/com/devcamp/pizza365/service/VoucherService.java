package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.Voucher;
import com.devcamp.pizza365.repository.IVoucherRepository;

@Service
public class VoucherService {
    @Autowired
    IVoucherRepository voucherRepository;
    public List<Voucher> getVouchers(){
        List<Voucher> listVoucher = new ArrayList<Voucher>();
        voucherRepository.findAll().forEach(listVoucher::add);
        return listVoucher;
    }
}
