package com.devcamp.pizza365.model;

public class CMenu {
    private String combo;
    private String duongKinh;
    private String suonNuong;
    private String salad;
    private int nuocNgot;
    private double thanhTien;
    public CMenu(String combo) {
        this.combo = combo;
    }
    public CMenu(String combo, String duongKinh, String suonNuong, String salad, int nuocNgot) {
        this.combo = combo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
    }
    public CMenu(String combo, String duongKinh, String suonNuong, String salad, int nuocNgot, double thanhTien) {
        this.combo = combo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }
    public String getCombo() {
        return combo;
    }
    public void setCombo(String combo) {
        this.combo = combo;
    }
    public String getDuongKinh() {
        return duongKinh;
    }
    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }
    public String getSuonNuong() {
        return suonNuong;
    }
    public void setSuonNuong(String suonNuong) {
        this.suonNuong = suonNuong;
    }
    public String getSalad() {
        return salad;
    }
    public void setSalad(String salad) {
        this.salad = salad;
    }
    public int getNuocNgot() {
        return nuocNgot;
    }
    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }
    public double getThanhTien() {
        return thanhTien;
    }
    public void setThanhTien(double thanhTien) {
        this.thanhTien = thanhTien;
    }
    @Override
    public String toString() {
        return "Menu [combo=" + combo + ", duongKinh=" + duongKinh + ", suonNuong=" + suonNuong + ", salad=" + salad
                + ", nuocNgot=" + nuocNgot + ", thanhTien=" + thanhTien + "]";
    }
     
}
