package com.devcamp.pizza365.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Customer;
import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.service.CustomerService;
import com.devcamp.pizza365.service.OrderService;
@RestController
@RequestMapping("/")
@CrossOrigin

public class CustomerOrderController {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private OrderService orderService;
    //get all customer list
    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomersApi(){
        try {
            return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    //get order by customer id
    @GetMapping("/customer-orders-query")
    public ResponseEntity<Set<Order>> getOrderByCustomerIdApi(@RequestParam(value = "customerId") long customerId){
        try {
            Set<Order> customerOrders = customerService.getOrderByCustomerId(customerId);
            if (customerOrders != null ){
                return new ResponseEntity<>(customerOrders, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //get all order list
    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrdersApi(){
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
